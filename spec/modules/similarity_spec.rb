require 'rails_helper'

class DummyClass
end

describe 'Similarity Util' do
  before(:each) do
    @dummy = DummyClass.new
    @dummy.extend Similarity
  end

  it 'should provide comparison distance between sitting and kitten' do
    distance = @dummy.levenshtein_distance "sitting", "kitten"
    expect(distance).to eq(3)
  end

  it 'should provide comparison distance between 2 emails' do
    distance = @dummy.levenshtein_distance "benoliv@salesloft.com", "benolive@salesloft.com"
    expect(distance).to eq(1)
  end

  it 'should provide comparison distance between 2 emails' do
    distance = @dummy.levenshtein_distance "knig0120@salesloft.com", "king0120@salesloft.com"
    expect(distance).to eq(2)
  end

  it 'should provide comparison distance between 2 emails' do
    distance = @dummy.levenshtein_distance "exxtraletter@salesloft.com", "extraletter@salesloft.com"
    expect(distance).to eq(1)
  end

  it 'should return length if one string is blank' do
    distance = @dummy.levenshtein_distance "hello", ""
    expect(distance).to eq(5)

    distance = @dummy.levenshtein_distance "", "hello"
    expect(distance).to eq(5)
  end
end