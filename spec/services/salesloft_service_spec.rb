require 'rails_helper'

describe 'Salesloft API' do
  request_data = { headers: {
    'Accept' => '*/*',
    'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
    'Authorization' => 'Bearer ' + ENV['SALESLOFT_APPLICATION_ID']
  } }

  describe 'get_people' do
    it 'should successfully retrieve people from the api' do
      test_data = File.open('./spec/support/fixtures/people.json').read
      stub_request(:get, 'https://api.salesloft.com/v2/people.json?include_paging_counts=true&page=1')
        .with(request_data)
        .to_return(status: 200, body: test_data, headers: {})

      actual = SalesloftService.new.get_people

      expected = JSON.parse(test_data)['data']
      expect(actual).to eq(expected)
    end

    it 'should fail with a server error' do
      stub_request(:get, 'https://api.salesloft.com/v2/people.json?include_paging_counts=true&page=1')
        .with(request_data)
        .to_return(status: [500, 'Internal Server Error'])

      expect do
        SalesloftService.new.get_people
      end.to raise_error SalesloftServiceError
    end
  end

end
