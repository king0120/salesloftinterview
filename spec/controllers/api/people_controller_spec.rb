require 'rails_helper'

RSpec.describe Api::PeopleController, type: :controller do
  describe 'GET index' do
    it 'should retrieve and return person data from Salesloft API' do
      test_data = File.read('./spec/support/fixtures/people.json')

      stub_request(:get, "https://api.salesloft.com/v2/people.json?include_paging_counts=true&page=1")
          .to_return(status: 200, body: test_data)

      get :index
      expected = [
          {"id":123456,"display_name":"Marisa Casper","title":"Direct Security Representative","email_address":"abcd@efg.net"},
          {"id":101694794,"display_name":"Griffin Rex","title":"International Test Agent","email_address":"1asd@qwerty.info"}
      ]

      expect(response.body).to eq(expected.to_json)
      expect(response.status).to eq(200)
    end

    it 'should fail' do
      stub_request(:get, "https://api.salesloft.com/v2/people.json?include_paging_counts=true&page=1")
          .to_return(status: 500)
      get :index
      expected = {error: 'Failure to connect to Salesloft API'}.to_json
      expect(response.body).to eq(expected)
      expect(response.status).to eq(500)
    end
  end
end
