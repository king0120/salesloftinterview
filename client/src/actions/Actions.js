import * as actions from './Constants'

export function receiveMe(me) {
  return {
    type: actions.RECEIVE_ME,
    me
  }
}

export function getPeople(people) {
  return {
    type: actions.GET_PEOPLE,
    people
  }
}

export function getDuplicates(dupes, email) {
  return {
    type: actions.GET_DUPLICATES,
    email,
    dupes
  }
}

export function error(err) {
  return {
    type: actions.ASYNC_ERROR,
    err
  }
}

export function increasePage() {
  return {
    type: actions.INCREASE_PAGE,
  }
}

export function decreasePage() {
  return {
    type: actions.DECREASE_PAGE,
  }
}