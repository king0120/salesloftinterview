import * as actions from './Actions'
import axios from 'axios'

export const creds = { credentials: 'same-origin' }

export const fetchPeople = () => async (dispatch, getState) => {
  try {
    const people = getState().people
    const page = people.page || 1
    const res = await axios.get(`/api/people?page=${page}`)
    dispatch(actions.getPeople(res.data))
  } catch (err) {
    dispatch(actions.error(err))
  }
}


export const checkForDuplicates = (email) => async (dispatch, getState) => {
  try {
    const res = await axios.get(`/api/people/find_matches?email=${email}`)
    dispatch(actions.getDuplicates(res.data, email))
  } catch (err) {
    dispatch(actions.error(err))
  }
}
