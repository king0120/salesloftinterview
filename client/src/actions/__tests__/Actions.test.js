import * as actions from '../Actions'

describe('Synchronous Redux Actions', () => {
  test('RECEIVE_ME', () => {
    const actual = actions.receiveMe({name: "Jamie"})
    expect(actual).toEqual({
      type: "RECEIVE_ME",
      me: {name: "Jamie"}
    })
})

  test('ASYNC_ERROR', () => {
      const mockError = {
        status: 418,
        message: "Something has gone horribly wrong"
      }
      const actual = actions.error(mockError)
      expect(actual).toEqual({
        type: "ASYNC_ERROR",
        err: mockError
      })
  })

  test('GET_PEOPLE', () => {
    const mockPeople = [{
      name: 'bob'
    }, {
      name: 'jim'
    }]
    const actual = actions.getPeople(mockPeople)

    expect(actual).toEqual({
      type: "GET_PEOPLE",
      people: mockPeople
    })
  })

  test('GET_DUPLICATES', () => {
    const actual = actions.getDuplicates(["rest@rest.met", 'terst@test.net'],"test@test.net")

    expect(actual).toEqual({
      type: "GET_DUPLICATES",
      dupes: ["rest@rest.met", "terst@test.net"],
      email: "test@test.net"
    })
  })

  test('INCREASE_PAGE', () => {
    const actual = actions.increasePage()

    expect(actual).toEqual({
      type: "INCREASE_PAGE"
    })
  })

  test('DECREASE_PAGE', () => {
    const actual = actions.decreasePage()

    expect(actual).toEqual({
      type: "DECREASE_PAGE"
    })
  })
})
