import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import * as actions from '../AsyncActions'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const mockAxios = new MockAdapter(axios)

describe('Async Actions', () => {
  let store
  beforeEach(() => {
    store = mockStore({people: {}})
  })

  describe('Get People from Rails API', () => {
    test('success', async () => {
      mockAxios.onGet('/api/people?page=1').reply(200, [{name: 'jim'}, {name: 'bob'}])

      await store.dispatch(actions.fetchPeople())

      const expected = [
        {"people": [{"name": "jim"}, {"name": "bob"}], "type": "GET_PEOPLE"}
      ]
      expect(store.getActions()).toEqual(expected)

    })

    test('failure', async () => {
      mockAxios.onGet('/api/people?page=1').reply(500)
      await store.dispatch(actions.fetchPeople())

      const expected =  [{"err": Error("Request failed with status code 500"), "type": "ASYNC_ERROR"}]
      expect(store.getActions()).toEqual(expected)
    })
  })

  describe('check for duplicates', () => {
    test('success', async () => {
      mockAxios.onGet('/api/people/find_matches?email=foo@bar.com').reply(200, [{foo: 'bar'}])

      await store.dispatch(actions.checkForDuplicates("foo@bar.com"))

      const expected = [{"dupes": [{"foo": "bar"}], "email": "foo@bar.com", "type": "GET_DUPLICATES"}]
      expect(store.getActions()).toEqual(expected)

    })

    test('failure', async () => {
      mockAxios.onGet('/api/people/find_matches?email=foo@bar.com').reply(500)
      await store.dispatch(actions.fetchPeople())

      const expected =  [{"err": Error("Request failed with status code 500"), "type": "ASYNC_ERROR"}]
      expect(store.getActions()).toEqual(expected)
    })
  })

})
