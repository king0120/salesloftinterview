import configureStore from '../configureStore'

describe('Configure Store', () => {
  it('should create new instance of Redux Store', () => {
    const store = configureStore()

    expect(store.dispatch).not.toBeUndefined()
    expect(store.getState).not.toBeUndefined()
  })
})