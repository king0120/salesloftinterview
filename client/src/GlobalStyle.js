import { createGlobalStyle } from 'styled-components'

export const theme = {
  main: {
    colors: {
      grey: 'rgba(0,0,0,.1)',
      white: 'rgba(255,255,255,1)',
      primary: '#1F85C0',
      medium: '#403075',
      light: '#887CAF',
      medium_light: '#615192',
      medium_dark: '#261758',
      dark: '#13073A'
    },
    mobileWidth: '500px',
    shadow: '0 1px 5px rgba(0, 0, 0, 0.46)'
  }
}

const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Roboto', sans-serif;
    background: ${props => props.theme.main.colors.grey};
  }

  #root {
    display: grid;
    grid-template: 66px 1fr / 1fr;
    height: 100%;
    width: 100%;
  }
`

export default GlobalStyle
