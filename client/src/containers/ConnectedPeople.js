import { connect } from 'react-redux'

import People from '../components/People'
import { increasePage, decreasePage } from '../actions/Actions'
import { fetchPeople, checkForDuplicates } from '../actions/AsyncActions'

const mapStateToProps = state => ({ people: state.people.all, page: state.people.page });

export const ConnectedPeople = connect(
  mapStateToProps,
  { fetchPeople, increasePage, decreasePage, checkForDuplicates }
)(People);

export default ConnectedPeople
