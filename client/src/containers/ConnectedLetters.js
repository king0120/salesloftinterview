import { connect } from 'react-redux'

import Letters from '../components/Letters'

const mapStateToProps = state => ({ letters: state.people.letters });

export const ConnectedLetters = connect(
  mapStateToProps,
)(Letters);

export default ConnectedLetters
