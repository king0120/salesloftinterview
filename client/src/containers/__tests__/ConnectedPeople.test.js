import React from 'react'
import { shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import ConnectedPeople from '../ConnectedPeople'
import People from '../../components/People'

describe('ConnectedPeople Redux Container', () => {
  let wrapper

  beforeEach(() => {
    const store = configureStore()
    const testState = {people: {all: [{val:"TEST STATE"}]}}
    wrapper = shallow(<ConnectedPeople store={store(testState)}/>)
  })
  test('it should contain People component', () => {
    expect(wrapper.find(People).exists()).toEqual(true)
  })

  test('it should give initial state to people component', () => {
    expect(wrapper.find(People).prop('people')).toEqual([{val: "TEST STATE"}])
  })

  test('it should include given functions', () => {
    expect(wrapper.find(People).props().fetchPeople).not.toBeUndefined()
    expect(wrapper.find(People).props().increasePage).not.toBeUndefined()
    expect(wrapper.find(People).props().decreasePage).not.toBeUndefined()
    expect(wrapper.find(People).props().checkForDuplicates).not.toBeUndefined()
  })
})
