import React from 'react'
import { shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import ConnectedLetters from '../ConnectedLetters'
import Letters from '../../components/Letters'

describe('ConnectedLetters Redux Container', () => {
  let wrapper

  beforeEach(() => {
    const store = configureStore()
    const testState = {people: {letters: "FooBar"}}
    wrapper = shallow(<ConnectedLetters store={store(testState)}/>)
  })
  test('it should contain Letters component', () => {
    expect(wrapper.find(Letters).exists()).toEqual(true)
  })

})