import React from 'react'
import { shallow } from 'enzyme'
import Letters from '../Letters'

describe('Letters Component', () => {
  let wrapper 
  beforeEach(() => {
    const letters = {a: 3, b: 15, c: 6}
    wrapper = shallow(<Letters letters={letters}/>)
  })

  it('should show letters in order of greatest to least', () => {
    expect(wrapper.find('.letter').at(0).text()).toContain('15')
    expect(wrapper.find('.letter').at(0).text()).toContain('b')
    expect(wrapper.find('.letter').at(1).text()).toContain('6')
    expect(wrapper.find('.letter').at(1).text()).toContain('c')
    expect(wrapper.find('.letter').at(2).text()).toContain('3')
    expect(wrapper.find('.letter').at(2).text()).toContain('a')
  })
})