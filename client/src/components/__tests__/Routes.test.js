import React from 'react'
import { shallow } from 'enzyme'
import { Route } from 'react-router-dom'
import Routes from '../Routes'
import ConnectedPeople from '../../containers/ConnectedPeople'

describe('Routes Component', () => {
  test('It renders the correct route', () => {
    const wrapper = shallow(<Routes />)
    // Map each route within the Router into object with 
    // path as the Key and component as the value
    const routeMap = wrapper.find(Route).reduce((map, route) => {
      const routeProps = route.props()
      map[routeProps.path] = routeProps.component
      return map
    },{})

    expect(routeMap['/']).toEqual(ConnectedPeople)
  })
})