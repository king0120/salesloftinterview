import React from 'react'
import { shallow } from 'enzyme'
import Person from '../Person'

describe('Person Component', () => {
  let wrapper
  let checkDupes
  beforeEach(() => {
    const person = {
      display_name: "Foo Barr",
      email_address: "foo@example.com",
      title: "Developer"
    }
    checkDupes = jest.fn()
    wrapper = shallow(<Person person={person} checkForDuplicates={checkDupes}/>)
  })

  it('should render out info about person', () => {
    expect(wrapper.find('ListHeader').dive().text()).toEqual("Foo Barr")
    expect(wrapper.find('ListDescription').dive().text()).toContain("Developer")
  })

  it('should handle dupes click', () => {
    wrapper.find('Styled(Button)').simulate('click')
    expect(checkDupes).toHaveBeenCalled()
  })

  it('should render dupes if they exist', () => {
    const person = {
      display_name: "Foo Barr",
      email_address: "foo@example.com",
      title: "Developer",
      dupes: [{email_address: "boo@example.com"}, {email_address: "noo@example.com"}]
    }
    wrapper = shallow(<Person person={person}/>)
    console.log(wrapper.debug())
    expect(wrapper.contains(<p>2 duplicates found</p>)).toEqual(true)
    expect(wrapper.contains(<li>boo@example.com</li>)).toEqual(true)
    expect(wrapper.contains(<li>noo@example.com</li>)).toEqual(true)
  })
})
