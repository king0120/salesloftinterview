import React from 'react'
import { shallow } from 'enzyme'
import NavBar from '../NavBar'

describe('NavBar Component', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<NavBar />)
  })

  test('It renders into the DOM', () => {
    expect(wrapper.find('img').length).toEqual(1)
  })
})