import React from 'react'
import { shallow } from 'enzyme'
import App from '../App'
import GlobalStyle from '../../GlobalStyle'
import Routes from '../Routes';

describe('App Component', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<App />)
  })

  test('It renders into the DOM', () => {
    expect(wrapper.find(GlobalStyle).length).toEqual(1)
    expect(wrapper.find(Routes).length).toEqual(1)
  })
})