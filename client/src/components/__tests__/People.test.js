import React from 'react'
import { shallow } from 'enzyme'
import People from '../People'
import Person from '../Person'

describe('People Component', () => {
  let wrapper
  let testFunc
  beforeEach(() => {
    testFunc = jest.fn()
    const people = [{id:1, foo: "foo"}, {id:2, foo: "bar"}]
    wrapper = shallow(<People fetchPeople={testFunc} people={people}/>)
  })

  it('should call fetchPeople after mounting', () => {
    wrapper.instance().componentDidMount()
    expect(testFunc).toHaveBeenCalled()
  })

  it('should include child components', () => {
    expect(wrapper.find("PeopleHeader").length).toEqual(1)
    expect(wrapper.find("PeopleNav").length).toEqual(1)
    expect(wrapper.find("Letters").length).toEqual(0)
  })

  it('should display multiple people', () => {
    expect(wrapper.find(Person).length).toEqual(2)
  })

  it('should be able to handle toggleCount', () => {
    expect(wrapper.state().showLetterCount).toEqual(false)
    wrapper.find('PeopleHeader').simulate('click')
    expect(wrapper.state().showLetterCount).toEqual(true)
    wrapper.update()
    expect(wrapper.find("Connect(Letters)").length).toEqual(1)
  })

  it('should be able to handle handleNav', () => {
    const cb = jest.fn()
    wrapper.instance().handleNav(cb)
    expect(testFunc).toHaveBeenCalled()
    expect(cb).toHaveBeenCalled()
  })
})
