import React from 'react'
import Chart from './Chart'
import styled from 'styled-components'

const LettersStyle = styled.div`
  padding: 0 30px 0;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: inherit;
  height: 100%;
  overflow-y: scroll;
  
  .rv-xy-plot {
    align-self: center;
    
    @media (max-width: ${props => props.theme.main.mobileWidth}) {
      display: none;
    }
  } 
`

const LetterStyle = styled.div`
  strong {
    font-size: 1.15rem;
  }
`

const Letters = ({ letters }) => {
  const keys = Object.keys(letters).sort((a, b) => letters[b] - letters[a])

  const plotData = keys.reduce((acc, val) => {
    const point = {
      x: val,
      y: letters[val]
    }
    acc.push(point)
    return acc
  }, [])

  return (
    <LettersStyle>
      <div>
        <h3>Most Common Letters</h3>
        {keys.map(key => (
          <LetterStyle key={key} className='letter'>
            <strong>{key}</strong>: {letters[key]} matches
          </LetterStyle>
        ))}
      </div>
      <Chart data={plotData} />
    </LettersStyle>
  )
}

Letters.defaultProps = {
  letters: {}
}

export default Letters
