import { Button } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const PeopleHeaderStyle = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 0;
`

export function PeopleHeader (props) {
  return (
    <PeopleHeaderStyle>
      <h1>People</h1>
      <Button primary onClick={props.onClick}>
        {props.status} Letter Counts
      </Button>
    </PeopleHeaderStyle>
  )
}

PeopleHeader.propTypes = {
  onClick: PropTypes.func,
  status: PropTypes.string
}
