import React from 'react';
import styled from 'styled-components'
import logo from './white-logo.svg'

const NavStyles = styled.div`
  background: ${props => props.theme.main.colors.primary};
  display: flex;
  padding: 10px;
  align-items: center;

  img {
    height: 75%;
  }
`

const NavBar = () => {
  return (
    <NavStyles>
      <img src={logo} alt="Salesloft Logo"/>
    </NavStyles>
  );
};

export default NavBar;