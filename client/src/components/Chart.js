import React from 'react'
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalBarSeries,
  HorizontalGridLines,
  GradientDefs
} from 'react-vis'
import {theme} from '../GlobalStyle'


const Chart = ({data}) => {
  return (
    <XYPlot
        xType="ordinal"
        animation
        height={500}
        width={500}
        colorRange={["linear"]}
      >
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <GradientDefs>
          <linearGradient id="CoolGradient" x1="0" x2="0" y1="0" y2="1">
            <stop offset="0%" stopColor={theme.main.colors.primary} stopOpacity={0.60} />
            <stop offset="100%" stopColor={theme.main.colors.dark} stopOpacity={0.75} />
          </linearGradient>
        </GradientDefs>
        <VerticalBarSeries color={'url(#CoolGradient)'} data={data} />
      </XYPlot>
  );
};

export default Chart;
