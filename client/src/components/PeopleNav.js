import { Button, Icon } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const PageNavStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: baseline;
  
  && button {
    margin: 5px;
  }
`

export function PeopleNav (props) {
  return (
    <PageNavStyle>
      <Button icon
        onClick={props.back}
        disabled={props.page <= 1}
      >
        <Icon name={'step backward'}/>
      </Button>
      <h3>Page: {props.page}</h3>
      <Button icon
        onClick={props.forward}
        disabled={props.people.length === 0}
      >
        <Icon name={'step forward'}/>
      </Button>
    </PageNavStyle>
  )
}

PeopleNav.propTypes = {
  back: PropTypes.func,
  page: PropTypes.any,
  forward: PropTypes.func,
  people: PropTypes.arrayOf (PropTypes.any)
}
