import React from 'react'
import { ThemeProvider } from 'styled-components'

import NavBar from './NavBar'
import Routes from './Routes'
import GlobalStyle, {theme} from '../GlobalStyle'


const App = ({ store }) => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <NavBar />
      <Routes />
    </>
  </ThemeProvider>
)

export default App
