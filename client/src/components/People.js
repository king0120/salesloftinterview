import React, { Component } from 'react'
import { List } from 'semantic-ui-react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Person from './Person'
import Letters from '../containers/ConnectedLetters'
import { PeopleNav } from './PeopleNav'
import { PeopleHeader } from './PeopleHeader'

const PeopleStyles = styled.div`
  margin: 10px 15px;
  padding: 0px 20px;
  width: 98%;
  height: 98%;
  
  background: ${props => props.theme.main.colors.white};
  box-shadow: ${props => props.theme.main.shadow};

  .people-list {
    display: flex;
    width: 100%;
    height: 73vh;
    .inner-list {
      width: ${props => props.showLetters ? '40%' : 'inherit'};
      
      @media(max-width: ${props => props.theme.main.mobileWidth}) {
        height: 90%;
        display: ${props => props.showLetters ? 'none' : 'block'};
      }
    }
    
    .person-list{
      height: 100%;
      overflow-y: scroll;
    }
  }
  
  
  
  @media (max-width: ${props => props.theme.main.mobileWidth}){
    margin: 0;
    width: 100%;
    height: 100%;
  }
`

class People extends Component {
  static defaultProps = {
    people: [],
    page: 0,
    fetchPeople: () => {}
  }
  static propTypes = {
    people: PropTypes.arrayOf(PropTypes.shape()),
    page: PropTypes.number,
    fetchPeople: PropTypes.func
  }

  state = {
    showLetterCount: false
  }

  toggleCount = () => this.setState({ showLetterCount: !this.state.showLetterCount })

  componentDidMount() {
    this.props.fetchPeople()
  }

  handleNav = (directionCb) => {
    directionCb()
    this.props.fetchPeople()
  }

  render() {
    const status = this.state.showLetterCount ? "Hide" : "Show"

    return (
      <PeopleStyles showLetters={this.state.showLetterCount}>
        <PeopleHeader onClick={this.toggleCount} status={status}/>
        <div className="people-list">
          <div className='inner-list'>
            <List relaxed divided className="person-list">
              {this.props.people.map (person => (
                <Person key={person.id} person={person} checkForDuplicates={this.props.checkForDuplicates}/>
              ))}
            </List>
            <PeopleNav back={() => this.handleNav (this.props.decreasePage)} page={this.props.page}
                       forward={() => this.handleNav (this.props.increasePage)} people={this.props.people}/>
          </div>
          {this.state.showLetterCount && <Letters/>}
        </div>


      </PeopleStyles>
    )
  }
}

export default People
