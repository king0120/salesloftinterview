import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ConnectedPeople from '../containers/ConnectedPeople';

const Routes = () => {
  return (
    <Router>
        <Switch>
          <Route exact path="/" component={ConnectedPeople}/>
        </Switch>
      </Router>
  );
};

export default Routes;