import React from 'react';
import { List, Button } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const DupeButton = styled(Button)`
  && {
   background: ${props => props.theme.main.colors.light}; 
   color: ${props => props.theme.main.colors.white}; 
   
   &:active, &:hover, &:focus{
    background: ${props => props.theme.main.colors.medium_light};
    color: ${props => props.theme.main.colors.white};  
   }
  }
`

const Person = ({person, checkForDuplicates}) => {
  return (
    <List.Item>
      <List.Content floated={"right"}>
        <DupeButton onClick={() => checkForDuplicates(person.email_address)}>Duplicates</DupeButton>
      </List.Content>
      <List.Content>
        <List.Header as='a'>{person.display_name}</List.Header>
        <List.Description>
          <h5>{person.title}</h5>
        </List.Description>
      </List.Content>
      <List.Content>
        <p>{person.email_address}</p>
      </List.Content>

      <List.Content>
        {person.dupes && (
          <div>
            <p>{person.dupes.length} duplicates found</p>
            <ul>
              {person.dupes.map(dupe => (
                <li key={dupe.id}>{dupe.email_address}</li>
              ))}
            </ul>
          </div>
        )}
      </List.Content>

    </List.Item>
  )
}

Person.propTypes = {
  person: PropTypes.shape({
    display_name: PropTypes.string,
    title: PropTypes.string,
    email_address: PropTypes.string
  })
}

export default Person;
