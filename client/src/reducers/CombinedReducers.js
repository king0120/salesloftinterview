import { combineReducers } from 'redux'

import { people } from './People'

const Reducers = combineReducers({
  people
});

export default Reducers