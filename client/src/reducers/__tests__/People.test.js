import { people } from '../People'

describe('PeopleReducer', () => {
  it('should contain an initial state', () => {
    expect(people(undefined, {})).toEqual({
        all: [],
        letters: {},
        page: 1,
      })
  })

  it('should handle "GET_PEOPLE"', () => {
    const data = [{name: 'Jim'}, {name: 'Ted'}]
    const action = {
      type: 'GET_PEOPLE',
      people: data
    }

    expect(people(undefined, action).all).toEqual(data)
    expect(people(undefined, action).letters).toEqual({"d": 4, "e": 4, "f": 2, "i": 2, "n": 4, "u": 2})
  })

  it('should handle "GET_DUPLICATES"', () => {
    const startState = {
      all: [
        {
          name: 'Jim',
          email_address: 'no_dupes@blah.net'
        },
        {
          name: 'Ted',
          email_address: 'foo@bar.com'
        }
      ]
    }
    const action = {
      type: 'GET_DUPLICATES',
      dupes: [{
        email_address: "noo@bar.com"
      },
      {
        email_address: "soo@bar.com"
      }],
      email: 'foo@bar.com'
    }

    const expected = [
      {email_address: "no_dupes@blah.net",
        name: "Jim"
      },
      {
        email_address: "foo@bar.com",
        name: "Ted",
        dupes: [ {email_address: "noo@bar.com"}, {email_address: "soo@bar.com"} ]
      }
    ]

    expect(people(startState, action).all).toEqual(expected)

  })

  it('should handle "INCREASE_PAGE"', () => {
    const action = {
      type: 'INCREASE_PAGE'
    }
    expect(people(undefined, action).page).toEqual(2)
  })

  it('should handle "DECREASE_PAGE"', () => {
    const action = {
      type: 'DECREASE_PAGE'
    }
    expect(people({page: 2}, action).page).toEqual(1)
  })
})
