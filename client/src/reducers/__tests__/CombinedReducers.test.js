import { createStore } from 'redux'
import reducer from '../CombinedReducers'
import { people } from '../People'

describe('CombinedReducer', () => {
  test('It should contain all reducers in the application', () => {
    const store = createStore(reducer)

    expect(store.getState().people).toEqual(people(undefined, {}))
  })
})