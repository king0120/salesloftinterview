import { GET_PEOPLE, DECREASE_PAGE, INCREASE_PAGE, GET_DUPLICATES } from '../actions/Constants'
import { countLetters } from '../util'

const defaultState = {
  page: 1,
  all: [],
  letters: {}
}
export function people( state = defaultState, action ) {
  switch (action.type) {
    case DECREASE_PAGE:
      return {...state, page: --state.page}
    case INCREASE_PAGE:
      return {...state, page: ++state.page}
    case GET_DUPLICATES:
      const clonedAll = [...state.all]
      const possibleDupe = clonedAll.find(person => person.email_address === action.email)
      possibleDupe.dupes = action.dupes
      return {...state, all: clonedAll}
    case GET_PEOPLE:
      const allEmails = action.people.reduce((acc, person) => acc += person.email_address,'')
      return {...state, all: action.people, letters: countLetters(allEmails)}
    default:
      return state
  }
}
