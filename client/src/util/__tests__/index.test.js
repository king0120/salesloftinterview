import * as util from '../index.js'

describe('countLetters', () => {
  test('aabccc', () => {
    expect(util.countLetters("aabccc")).toEqual({
      c: 3,
      a: 2,
      b: 1,
    })
  })

  test('georgsee_aiaegwnd@boy312er.namddde', () => {
    const expected ={".": 1, "1": 1, "2": 1, "3": 1, "@": 1, "_": 1, "a": 3, "b": 1, "d": 4, "e": 6, "g": 3, "i": 1, "m": 1, "n": 2, "o": 2, "r": 2, "s": 1, "w": 1, "y": 1}
    expect(util.countLetters("georgsee_aiaegwnd@boy312er.namddde")).toEqual(expected)
  })
})
