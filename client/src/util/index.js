export const countLetters = (word) => {
  const histogram = word.split('').reduce((hist, letter) => {
    if (hist[letter]){
      hist[letter] += 1
    } else {
      hist[letter] = 1
    }
    return hist
  }, {})

  return histogram
}