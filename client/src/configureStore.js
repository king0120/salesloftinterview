import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducer from './reducers/CombinedReducers'

export default function configureStore() {
    //Injects Devtools and Thunk Middleware
    return createStore(
        reducer,
        composeWithDevTools(
            applyMiddleware(...[thunk]),
        ),
    )
}
