
module Similarity
  # Uses Levenshtein distance to calculate number of changes needed to make
  # two strings match
  # @return Int number of changes required to make strings match
  def levenshtein_distance(str1, str2)
    @str1 = str1
    @str2 = str2
    return @str1.length if @str2.empty?
    return @str2.length if @str1.empty?

    str1_chars = @str1.split('')
    str2_chars = @str2.split('')
    str1_length = @str1.length
    str2_length = @str2.length
    levenshtein_matrix = build_matrix str1_length, str2_length

    populate_matrix(levenshtein_matrix, str1_chars,
                    str1_length, str2_chars, str2_length)

    # Last cell in the matrix is the total distance between strings
    levenshtein_matrix[str2_length - 1][str1_length - 1]
  end

  private

  # Builds a 2D array with first row and column filled out.
  # These values are used to calculate the other cells in the matrix
  def build_matrix(length1, length2)
    matrix = Array.new(length2 + 1, 0)
    filled = matrix.map { Array.new length1 + 1, 0 }

    (0..length1).each { |i| filled[0][i] = i }
    (0..length2).each { |i| filled[i][0] = i }

    filled
  end

  def calculate_substitution_cost(val1, val2)
    val1 == val2 ? 0 : 1
  end

  # Used for debugging the matrix.
  def print_matrix(matrix)
    puts @str1
    puts @str2
    matrix.each { |r| puts r.to_s }
  end

  # For each cell in the matrix, determine whether a insertion, deletion, or substitution
  # needs to occur
  def populate_matrix(levenshtein_matrix, str1_chars,
                      str1_length, str2_chars, str2_length)
    (1..str2_length).each do |i|
      (1..str1_length).each do |j|
        sub_cost = calculate_substitution_cost str2_chars[i - 1], str1_chars[j - 1]
        deletion = levenshtein_matrix[i - 1][j] + 1
        insertion = levenshtein_matrix[i][j - 1] + 1
        substitution = levenshtein_matrix[i - 1][j - 1] + sub_cost

        levenshtein_matrix[i][j] = [deletion, insertion, substitution].min
      end
    end
  end
end
