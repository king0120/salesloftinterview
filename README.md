# SalesLoft Development Interview Starter Kit

This application uses Ruby on Rails and React (via Create-React-App) to present data retrieved from the SalesLoft People API.

### Deployed on Heroku
[Heroku App](https://king-salesloft-tech.herokuapp.com/)

### Running the App Locally

```
$ bundle install
$ cd client && npm i && cd ..
$ foreman start -f Procfile.dev
```

> You must provide a `.env` file that includes a variable named `SALESLOFT_APPLICATION_ID`. This is your Salesloft API key
