class SalesloftServiceError < StandardError
end

class SalesloftService
  attr_reader :connection
  def initialize
    @connection = Faraday.new('https://api.salesloft.com/') do |c|
      c.use Faraday::Response::RaiseError
      c.use Faraday::Adapter::NetHttp
    end
  end

  def make_secure_request(endpoint)
    puts endpoint
    connection.get do |req|
      req.url endpoint
      req.headers['Authorization'] = 'Bearer ' + ENV['SALESLOFT_APPLICATION_ID']
    end
  end

  def get_people(page = 1)
    page ||= 1
    response = make_secure_request("v2/people.json?page=#{page}&include_paging_counts=true").body
    json = JSON.parse(response)
    json['data']
  rescue Faraday::ClientError
    raise SalesloftServiceError, 'Failure to connect to Salesloft API'
  end

  def get_all_people
    result = Rails.cache.fetch("salesloft_people", expires_in: 1.minutes) do
      page = 1
      people = []
      until page.nil?
        response = make_secure_request("v2/people.json?page=#{page}&per_page=100&include_paging_counts=true").body
        json = JSON.parse(response)
        people += json['data']
        page = json['metadata']['paging']['next_page']
      end
      people.to_a
    end

    result
  end
end
