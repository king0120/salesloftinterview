class Api::PeopleController < ApplicationController
  include Similarity

  def index
    people = SalesloftService.new.get_people(params[:page])
    response = people.map do |person|
      {
        id: person['id'],
        display_name: person['display_name'],
        title: person['title'],
        email_address: person['email_address']
      }
    end
    json_response response
  rescue SalesloftServiceError => e
    json_response({ error: e.message }, 500)
  end

  def find_matches
    email = params[:email]
    people = SalesloftService.new.get_all_people

    matches = people.select do |person|
      distance = levenshtein_distance email, person['email_address']
      distance <= 3
    end

    json_response matches.reject { |match| match['email_address'] == email }
  end
end
